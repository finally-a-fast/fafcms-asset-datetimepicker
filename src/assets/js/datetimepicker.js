;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    var onCloseDatePicker = function(picker) {
        var newValue = luxon.DateTime.fromFormat(picker.$dateInput.val(), 'yyyy-MM-dd')

        if (newValue.invalid !== null) {
            newValue = luxon.DateTime.local()
        }

        picker.value = picker.value.set({'year': newValue.year, 'month': newValue.month, 'day': newValue.day})

        if (picker.pickerType === 'datetime') {
            openTimePicker(picker)
        }
    }

    var onCloseTimePicker = function(picker) {
        var newValue = luxon.DateTime.fromFormat(picker.$timeInput.val(), 'HH:mm')

        if (newValue.invalid !== null) {
            newValue = luxon.DateTime.local()
        }

        picker.value = picker.value.set({'hour': newValue.hour, 'minute': newValue.minute, 'second': newValue.second})
    }

    var openTimePicker = function(picker) {
        if (typeof M === 'undefined') {
            picker.$timeInput.pickatime({
                twelvehour: false,
                donetext: picker.i18n.time.done,
                cleartext: picker.i18n.time.clear,
                canceltext: picker.i18n.time.cancel,
                autoclose: false,
                ampmclickable: false,
                afterDone: function() {
                    onCloseTimePicker(picker)
                }
            })

            window.setTimeout(function () {
                picker.$timeInput.pickatime('show')
            }, 0)
        } else {
            picker.$timeInput.timepicker({
                twelveHour: false,
                i18n: picker.i18n.time,
                autoClose: false,
                onCloseEnd: function() {
                    onCloseTimePicker(picker)
                }
            })

            var timePicker = M.Timepicker.getInstance(picker.$timeInput[0])

            window.setTimeout(function () {
                timePicker.open()
            }, 0)
        }
    }

    $('body').on('click', '.trigger-picker', function(e) {
        var $btn = $(this)
        var picker = {
            inputId: $btn.data('picker-input'),
            pickerType: $btn.data('picker-type'),
            format: $btn.data('picker-format'),
            i18n: {
                date: {
                    today: 'Today',
                    clear: 'Clear',
                    close: 'OK',
                },
                time: {
                    done: 'OK',
                    clear: 'Clear',
                    cancel: 'Cancel',
                }
            },
            set value(newValue) {
                picker.$input.val(newValue.toFormat(this.format))
            },
            get value() {
                var oldValue = luxon.DateTime.fromFormat(picker.$input.val(), this.format)

                if (oldValue.invalid === null) {
                    return oldValue
                } else {
                    return luxon.DateTime.local()
                }
            }
        }

        picker.i18n = $.extend(picker.i18n, $btn.data('picker-i18n'))
        picker.$input = $('#' + picker.inputId)
        picker.$dateInput = $('#' + picker.inputId + '-date-picker-helper')
        picker.$timeInput = $('#' + picker.inputId + '-time-picker-helper')

        if (picker.$dateInput.length > 0) {
            picker.$dateInput.remove()
        }

        picker.$input.after('<input type="text" style="display:none;" id="' + picker.inputId + '-date-picker-helper">')
        picker.$dateInput = $('#' + picker.inputId + '-date-picker-helper')
        picker.$dateInput.val(picker.value.toFormat('yyyy-MM-dd'))

        if (picker.$timeInput.length > 0) {
            picker.$timeInput.remove()
        }

        picker.$input.after('<input type="text" style="display:none;" id="' + picker.inputId + '-time-picker-helper">')
        picker.$timeInput = $('#' + picker.inputId + '-time-picker-helper')
        picker.$timeInput.val(picker.value.toFormat('HH:mm'))

        if (picker.pickerType === 'date' || picker.pickerType === 'datetime') {
            var dateSet = false

            if (typeof M === 'undefined') {
                picker.$dateInput.pickadate({
                    selectMonths: true,
                    selectYears: 300,
                    today: picker.i18n.date.today,
                    clear: picker.i18n.date.clear,
                    close: picker.i18n.date.close,
                    closeOnSelect: false,
                    format: 'yyyy-mm-dd',
                    onClose: function() {
                        if (dateSet) {
                            onCloseDatePicker(picker)
                        }
                    },
                    onSet: function() {
                        dateSet = true
                    }
                })

                var datePicker = picker.$dateInput.pickadate('picker')
            } else {
                picker.$dateInput.datepicker({
                    yearRange: 300,
                    i18n: picker.i18n.date,
                    closeOnSelect: false,
                    format: 'yyyy-mm-dd',
                    onClose: function() {
                        if (dateSet) {
                            onCloseDatePicker(picker)
                        }
                    },
                    onSelect: function() {
                        dateSet = true
                    }
                })

                var datePicker = M.Datepicker.getInstance(picker.$dateInput[0])
            }

            window.setTimeout(function () {
                datePicker.open()
            }, 0)
        } else {
            openTimePicker(picker)
        }
    })

    return fafcms
})
