<?php
namespace fafcms\assets\datetimepicker;

use yii\web\AssetBundle;

class DateTimePickerAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';

    public $js = [
        'js/datetimepicker.js',
    ];

    public $depends = [
        'fafcms\assets\init\InitAsset'
    ];
}
